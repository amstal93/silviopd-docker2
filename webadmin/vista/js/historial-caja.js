jQuery(document).ready(function() {
    listar();
});

function listar() {

    var ruta = DIRECCION_WS + "reporte.almacen.anular.listar.php";

    $.post(ruta, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<table id="responsive-datatable" class="table table-bordered table-bordered dt-responsive nowrap" cellspacing="0" width="100%">';
            html += '<thead>';
            html += '<tr>';
            html += '<th>N° Registro</th>';
            html += '<th>N°</th>';
            html += '<th>Cantidad</th>';
            html += '<th>Operación</th>';
            html += '<th>Cliente</th>';
            html += '<th>Tipo</th>';
            html += '<th>Ubicacion</th>';
            //html += '<th>Precio</th>';
            html += '<th>SubTotal</th>';
            html += '<th>Fecha y Hora</th>';
            html += '<th class="text-center">Opciones</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            //Detalle
            $.each(datosJSON.datos, function(i, item) {
                html += '<tr>';
                html += '<td>' + item.id_registro + '</td>';
                html += '<td>' + item.id_historial + '</td>';
                html += '<td>' + item.cantidad + '</td>';
                if (item.operacion === 'INGRESO') {
                    html += '<td><span class="badge2 label-table badge-success"><i class="mdi mdi-arrow-up-thick"></i>INGRESO</span></td>';
                } else {
                    html += '<td><span class="badge2 label-table badge-danger"><i class="mdi mdi-arrow-down-thick"></i>SALIDA</span></td>';
                }
                html += '<td>' + item.nombre_cliente + '</td>';
                html += '<td>' + item.nombre_caja + '</td>';
                html += '<td>' + item.nombre_ubicacion + '</td>';
                //html += '<td>' + item.precio_base + '</td>';
                html += '<td>' + item.sub_total + '</td>';
                html += '<td>' + item.fecha + ' - ' + item.hora + '</td>';
                html += '<td class="text-center"><button type="button" class="btn btn-sm btn-icon waves-effect waves-light btn-danger" onclick="anular(' + "" + item.id_registro + "," + item.id_historial + ')"> <i class="fa fa-remove"></i> </button></td>';
                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';

            $("#listado").html(html);

            $('#responsive-datatable').DataTable({
                "language": {

                    "decimal": "",
                    "emptyTable": "No existe datos",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty": "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered": "(filtrado de _MAX_ total registros)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "No se encontro coincidencias",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "aria": {
                        "sortAscending": ": columna ascedente",
                        "sortDescending": ": columna descente"
                    }

                },
                "order": [
                    [0, "desc"],
                    [1, "desc"],
                ]
            });
            $(".pre-loader").fadeOut("slow");
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}



function anular(id_registro, id_historial) {

    var ruta = DIRECCION_WS + "historial.caja.anular.php";
    var dni_user = Cookies.get('id_usuario_area');
    var tabla = 3;
    swal({
        title: '¿Desea Anular?',
        text: "se Anulara el registro!",
        showCancelButton: true,
        confirmButtonClass: 'btn btn-confirm mt-2',
        cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
        confirmButtonText: 'Anular',
        cancelButtonText: 'cancelar',
        imageUrl: "../vista/imagenes/pregunta.png"
    }).then(function() {
        $.post(ruta, { p_tabla: tabla, p_id: id_historial, p_id_user: dni_user, p_nro_placa: id_registro }, function() {}).done(function(resultado) {
            var datosJSON = resultado;
            if (datosJSON.estado === 200) {
                swal({
                    title: 'EXITO!',
                    text: datosJSON.mensaje,
                    type: 'success',
                    confirmButtonClass: 'btn btn-confirm mt-2'
                });
                listar();
            } else {
                swal("Mensaje del sistema", resultado, "warning");
            }
        }).fail(function(error) {
            var datosJSON = $.parseJSON(error.responseText);
            swal("Error", datosJSON.mensaje, "error");
        })
    });
}